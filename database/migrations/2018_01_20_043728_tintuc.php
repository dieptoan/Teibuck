<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tintuc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tintuc', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUser');
            $table->string('Link');
            $table->string('NoiDung');
            $table->string('Image');
            $table->integer('NoiBat');
            $table->integer('SoLuotXem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tintuc');        
    }
}
