<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeed::class);
        $this->call(TinTucSeed::class);
        $this->call(CommentSeed::class);
    }
}
class UsersSeed extends Seeder
{
    public function run(){
        DB::table('users')->insert([
            ['image'=> '12.jpg','name'=> 'Dan Thy','email' => 'thy@gmail.com','password'=> bcrypt('danthy'),'created_at' => new DateTime()],
            ['image'=> '12.jpg','name'=> 'Khanh Tran','email' => 'tran@gmail.com','password'=> bcrypt('khanhtran'),'created_at' => new DateTime()],
            ['image'=> '12.jpg','name'=> 'Diep Toan','email' => 'toan@gmail.com','password'=> bcrypt('dieptoan'),'created_at' => new DateTime()],
            ['image'=> 'avatar_nguyen.png','name'=> 'Nguyen Vo','email' => 'nguyen@gmail.com','password'=> bcrypt('nguyen'),'created_at' => new DateTime()],
            ['image'=> 'avatar_nguyen.png','name'=> 'Kiet Hi','email' => 'kiet@gmail.com','password'=> bcrypt('kiet'),'created_at' => new DateTime()],
        ]);
    }
}
class TinTucSeed extends Seeder
{
    public function run(){
        DB::table('tintuc')->insert([
            [   'idUser' => 2,
                'Link' => 'news1',
                'NoiDung' => 'Thế gian này ai trẻ trâu bằng lzz Dung',
                'Image' => '1.jpg',
                'NoiBat' => 1,
                'SoLuotXem'=> 20,
                'created_at' => new DateTime()
            ],
                [   'idUser' => 3,
                'Link' => 'news2',
                'NoiDung' => 'Lzz gương dơ quá đ thấy..',
                'Image' => '1.jpg',
                'NoiBat' => 1,
                'SoLuotXem'=> 20,
                'created_at' => new DateTime()
            ],
            [   'idUser' => 3,
            'Link' => 'news3',
            'NoiDung' => '2nd mà được đi Tây Bắc',
            'Image' => '3.jpg',
            'NoiBat' => 3,
            'SoLuotXem'=> 200,
            'created_at' => new DateTime()
            ],
            [   'idUser' => 1,
            'Link' => 'news4',
            'NoiDung' => 'Cắm cái cờ cái được không',
            'Image' => '4.jpg',
            'NoiBat' => 4,
            'SoLuotXem'=> 140,
            'created_at' => new DateTime()
        ],
            [   'idUser' => 2,
            'Link' => 'news5',
            'NoiDung' => '2 thằng lzz',
            'Image' => '5.jpg',
            'NoiBat' => 5,
            'SoLuotXem'=> 40,
            'created_at' => new DateTime()
        ],
            [   'idUser' => 3,
            'Link' => 'news6',
            'NoiDung' => 'Cô đừng đi lấy chồng...',
            'Image' => '6.jpg',
            'NoiBat' => 6,
            'SoLuotXem'=> 320,
            'created_at' => new DateTime()
            ],
            [   'idUser' => 3,
            'Link' => 'news7',
            'NoiDung' => 'Cười cái chớ',
            'Image' => 'xFx_DSC03653.JPG',
            'NoiBat' => 1,
            'SoLuotXem'=> 200,
            'created_at' => new DateTime()
            ],
            [   'idUser' => 4,
            'Link' => 'news8',
            'NoiDung' => 'Lzz Cường',
            'Image' => 'dcZ_9.JPG',
            'NoiBat' => 1,
            'SoLuotXem'=> 230,
            'created_at' => new DateTime()
        ]
        ]);
    }
}
class CommentSeed extends Seeder
{
    public function run(){
        DB::table('comment')->insert([
            ['idUser'=> 3,'idTinTuc' => 2,'NoiDung'=> 'hahaha','created_at' => new DateTime()],
            ['idUser'=> 1,'idTinTuc' => 3,'NoiDung'=> 'Dan Thy da comment','created_at' => new DateTime()],
            ['idUser'=> 2,'idTinTuc' => 1,'NoiDung'=> 'ádasd','created_at' => new DateTime()],
            ['idUser'=> 2,'idTinTuc' => 1,'NoiDung'=> 'hahahahahaha','created_at' => new DateTime()],
            ['idUser'=> 3,'idTinTuc' => 3,'NoiDung'=> 'Hài clm','created_at' => new DateTime()],
            ['idUser'=> 3,'idTinTuc' => 3,'NoiDung'=> 'haha','created_at' => new DateTime()],
            ['idUser'=> 5,'idTinTuc' => 3,'NoiDung'=> 'Ahaha','created_at' => new DateTime()],
            ['idUser'=> 4,'idTinTuc' => 2,'NoiDung'=> 'Đ hiểu','created_at' => new DateTime()],
            ['idUser'=> 3,'idTinTuc' => 3,'NoiDung'=> 'Nụ cười PS','created_at' => new DateTime()]
        ]);
    }
}

