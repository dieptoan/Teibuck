<?php

use Illuminate\Database\Seeder;

class TinTucSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tintuc')->insert([
                        [   'idUser' => 2,
                            'Link' => 'news1',
                            'NoiDung' => 'Thế gian này ai trẻ trâu bằng lzz Dung',
                            'Image' => '1.jpg',
                            'NoiBat' => 1,
                            'SoLuotXem'=> 20,
                            'created_at' => new DateTime()
                        ],
                            [   'idUser' => 3,
                            'Link' => 'news2',
                            'NoiDung' => 'Lzz gương dơ quá đ thấy..',
                            'Image' => '1.jpg',
                            'NoiBat' => 1,
                            'SoLuotXem'=> 20,
                            'created_at' => new DateTime()
                        ],
                        [   'idUser' => 3,
                        'Link' => 'news3',
                        'NoiDung' => '2nd mà được đi Tây Bắc',
                        'Image' => '3.jpg',
                        'NoiBat' => 3,
                        'SoLuotXem'=> 200,
                        'created_at' => new DateTime()
                        ],
                        [   'idUser' => 1,
                        'Link' => 'news4',
                        'NoiDung' => 'Cắm cái cờ cái được không',
                        'Image' => '4.jpg',
                        'NoiBat' => 4,
                        'SoLuotXem'=> 140,
                        'created_at' => new DateTime()
                    ],
                        [   'idUser' => 2,
                        'Link' => 'news5',
                        'NoiDung' => '2 thằng lzz',
                        'Image' => '5.jpg',
                        'NoiBat' => 5,
                        'SoLuotXem'=> 40,
                        'created_at' => new DateTime()
                    ],
                        [   'idUser' => 3,
                        'Link' => 'news6',
                        'NoiDung' => 'Cô đừng đi lấy chồng...',
                        'Image' => '6.jpg',
                        'NoiBat' => 6,
                        'SoLuotXem'=> 320,
                        'created_at' => new DateTime()
                        ],
                        [   'idUser' => 3,
                        'Link' => 'news7',
                        'NoiDung' => 'Cười cái chớ',
                        'Image' => 'xFx_DSC03653.JPG',
                        'NoiBat' => 1,
                        'SoLuotXem'=> 200,
                        'created_at' => new DateTime()
                        ],
                        [   'idUser' => 6,
                        'Link' => 'news8',
                        'NoiDung' => 'Lzz Cường',
                        'Image' => 'dcZ_9.JPG',
                        'NoiBat' => 1,
                        'SoLuotXem'=> 230,
                        'created_at' => new DateTime()
                    ],
                    ]);
    }
}
